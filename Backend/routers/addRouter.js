const { CANCELLED } = require('dns');
const express = require('express');
const router = express.Router();
const addController = require('../controllers/addController');
const fs = require('fs');

router.post('/:id/:character/:category/:part', addController.postNewItem);
router.post('/:id/:character', addController.postNewCharacter);

module.exports = router;