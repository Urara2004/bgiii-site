const { CANCELLED } = require('dns');
const express = require('express');
const router = express.Router();
const fs = require('fs');
const writer = require('../utils/writeToFile');
const { post } = require('../controllersOld/patchCharacter');

router.use((req, res, next) => {
    next();
})

async function postNewItem(req, res) {
    const file = require(writer.fileName);
    if (req.params.category == 'gear') {
        file[req.params.id][req.params.character].gear[req.params.part] = req.body;
    } else {
        file[req.params.id][req.params.character][req.params.category][req.params.part].push(req.body);
    }
    writer.writeToFile(writer.fileName, file, res);
}

async function postNewCharacter(req, res) {
    const file = require(writer.fileName);
    console.log(req.body);
    console.log(file[req.params.id]);
    file[req.params.id][req.params.character].push(req.body);
    console.log(file);
}

module.exports = {
    postNewItem,
    postNewCharacter
}