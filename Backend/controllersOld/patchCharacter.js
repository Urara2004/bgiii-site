const express = require('express');
const router = express.Router();
const fs = require('fs');

let index;

router.use((req, res, next) => {
    next();
})

function writeToFile(fileName, file) {
    fs.writeFileSync(fileName, JSON.stringify(file, null, 2), (error) => {
        if (error) {
            console.log("An error has occurred ", error);
            return;
        }
    })
}

router.patch('/:character/attributes', (req, res, next) => {
    const fileName = 'D:/BG3 site/Backend/JSON/' + req.params.character + '.json';
    const file = require(fileName);
    file.attributes = req.body;
    writeToFile(fileName, file);
})

router.patch('/:character/gear/:part', (req, res, next) => {
    const fileName = 'D:/BG3 site/Backend/JSON/' + req.params.character + '.json';
    const file = require(fileName);
    file.gear[req.params.part] = req.body;
    writeToFile(fileName, file);
})

router.patch('/:character/:category/:part/:name', (req, res, next) => {
    const fileName = 'D:/BG3 site/Backend/JSON/' + req.params.character + '.json';
    const file = require(fileName);
    containsName(file[req.params.category][req.params.part], req.params.name)
    file[req.params.category][req.params.part][index] = req.body;
    writeToFile(fileName, file);
})

function containsName(list, name) {
    for (var i = 0; i < list.length; i++) {
        if (list[i].name == name) {
            index = i;
            return true;
        }
    }
    return false;
}

module.exports = router;