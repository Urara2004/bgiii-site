const express = require('express');
const router = express.Router();
const fs = require('fs');

let index;

router.use((req, res, next) => {
    next();
})

function writeToNull(fileName, file, res) {
    try {
        fs.writeFileSync(fileName, JSON.stringify(file, null, 2), (error) => {
            if (error) {
                console.log("An error has occurred ", error);
                return;
            }
        })
        res.status(200).send({});
    } catch (error) {
        console.log(error);
        res.status(404);
        return;
    }
}

router.delete('/:character/:category/:part/:name', (req, res, next) => {
    const fileName = 'D:/BG3 site/Backend/JSON/' + req.params.character + '.json';
    const file = require(fileName);
    if (req.params.category == 'gear') {
        if (contains(file.gear, req.params.part)) {
            file.gear[req.params.part] = null;
            writeToNull(fileName, file, res);
        } else {
            console.log("Nothing by that name exists")
        }
    } else if (req.params.category == 'skills') {
        if (contains(file.skills, req.params.part)) {
            if (containsName(file.skills[req.params.part], req.params.name)) {
                file.skills[req.params.part].splice(index, 1);
                writeToNull(fileName, file, res);
            } else {
                console.log("Nothing by that name exists")
            }
        } else {
            console.log("Nothing by that part exists")
        }
    } else if (req.params.category == 'spells') {
        if (contains(file.spells, req.params.part)) {
            if (containsName(file.spells[req.params.part], req.params.name)) {
                file.spells[req.params.part].splice(index, 1);
                writeToNull(fileName, file, res);
            } else {
                console.log("Nothing by that name exists")
            }
        } else {
            console.log("Nothing by that part exists")
        }
    }
})

function contains(list, object) {
    for (let i in list) {
        if (i === object) {
            return true;
        }
    }
    return false;
}

function containsName(list, name) {
    for (var i = 0; i < list.length; i++) {
        if (list[i].name == name) {
            index = i;
            return true;
        }
    }
    return false;
}

module.exports = router;