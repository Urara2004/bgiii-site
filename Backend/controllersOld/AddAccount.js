const { CANCELLED } = require('dns');
const express = require('express');
const router = express.Router();
const fs = require('fs');

router.use((req, res, next) => {
    next();
})

function writeToFile(fileName, file, res) {
    try {
        fs.writeFileSync(fileName, JSON.stringify(file, null, 2), (error) => {
            if (error) {
                console.log("An error has occurred ", error);
                return;
            }
        })
        res.status(201).send({});
    } catch (error) {
        console.log(error);
        res.status(500);
        return;
    }
}

router.post('/:username', (req, res, next) => {
    const fileName = 'D:/BG3 site/Backend/JSON/AllCharacters.json';
    const file = require(fileName);
    const newID = parseInt(Object.keys(file).sort().pop()) + 1;
    file[newID] = {"username": req.params.username};
    writeToFile(fileName, file, res);
});

module.exports = router;