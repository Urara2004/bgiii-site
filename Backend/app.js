const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const path = require('path');
const app = express();
const port = 5000;
const router = express.Router();

const updateCharacter = require('./controllersOld/patchCharacter.js');
const addPart = require('./routers/addRouter.js');
const addAccount = require('./controllersOld/AddAccount.js');
const deletePart = require('./controllersOld/delete.js');

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}))

app.use('/update', updateCharacter);
app.use('/add', addPart);
app.use('/newAccount', addAccount);
app.use('/delete', deletePart);
app.use('/json', express.static(path.join(__dirname, 'JSON')))

app.listen(port, (error) => {
    if (!error) {
        console.log("Server is running and app is listening to port " + port);
    } else {
        console.log("Error occurred, server can't start, ", error);
    }
})

app.get('/', (req, res) => {
    res.status(200);
    res.send("Baldur's Gate III build website");
});

app.get('/json/:character', (req, res) => {})