const { CANCELLED } = require('dns');
const express = require('express');
const router = express.Router();
const fs = require('fs');

const fileName = 'D:/BGIII site/Backend/JSON/AllCharacters.json';

function writeToFile(fileName, file, res) {
    try {
        fs.writeFileSync(fileName, JSON.stringify(file, null, 2), (error) => {
            if (error) {
                console.log("An error has occurred ", error);
                return;
            }
        })
        res.status(201).send({});
    } catch (error) {
        console.log(error);
        res.status(500);
        return;
    }
}

module.exports = {
    writeToFile,
    fileName
};