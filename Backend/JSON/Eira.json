{
  "attributes": {
    "name": "Eira",
    "gender": "Female",
    "race": "Elf",
    "subrace": "Wood Elf",
    "class": "Sorcerer",
    "subclass": "Wild Magic",
    "level": 5
  },
  "gear": {
    "helmet": null,
    "robe": null,
    "armour": null,
    "gloves": {
      "name": "The Sparkle Hands",
      "effect": [
        "Conductive Strikes: On a hit with an unarmed attack, the wearer gains 2 Lightning Charges",
        "Effective Transmitter: While imbued with Lightning Charges, attacks against metal constructs and foes wearing metal armour gain Advantage"
      ]
    },
    "shoes": {
      "name": "Springstep Boots",
      "effect": "Swift Strides: When the wearer Dashes or takes a similar action during combat, they gain Momentum for 3 turns"
    },
    "necklace": {
      "name": "Psychic Spark",
      "effect": "Psychic Missiles: Shoot an additional dart whenever you cast Magic Missile",
      "spell": {
        "name": "Magic Missile",
        "damage": "6-15 Force",
        "description": "Shoot 3 magical darts, each dealing 2-5 Force damage. They always hit their target",
        "duration": null,
        "range": 18,
        "cooldown": "Long Rest",
        "cost": null,
        "action": "Action"
      }
    },
    "ring 1": null,
    "ring 2": null,
    "melee 1": null,
    "melee 2": null,
    "ranged 1": {
      "name": "Spellthief",
      "damage": "2-9 Piercing",
      "effect": "Arcane Vehemence: Once per Short Rest, you regain a Level 1 spell slot when you land a Critical Hit with the Spellthief"
    },
    "ranged 2": null
  },
  "skills": {
    "melee": [
      {
        "name": "Main Hand Attack",
        "damage": [
          "1-8 Bludgeoning",
          "1-4 Cold"
        ],
        "description": "Make a melee attack with your equipped weapon",
        "duration": null,
        "area": null,
        "range": 0,
        "cooldown": null,
        "save": null,
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Topple",
        "damage": "1-4 Bludgeoning",
        "description": "Swipe at a creature to knock it Prone",
        "duration": 1,
        "area": null,
        "range": 0,
        "cooldown": "Short Rest",
        "save": "Dexterity",
        "concentration": false,
        "action": "Action"
      }
    ],
    "ranged": [
      {
        "name": "Ranged Attack",
        "damage": "2-9 Piercing",
        "description": "Make a ranged attack with your equipped weapon",
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": null,
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Hamstring Shot",
        "damage": "2-9 Piercing",
        "description": "Shoot an enemy in the thigh and possibly reduce their movement speed by 50%",
        "duration": 2,
        "area": null,
        "range": 18,
        "cooldown": "Short Rest",
        "save": "Constitution",
        "concentration": false,
        "action": "Action"
      }
    ],
    "passive": [
      {
        "name": "Brace (Ranged)",
        "damage": null,
        "description": [
          "Spend 6m of your movement speed",
          "For the rest of your turn, roll ranged damage twice and use the highest result"
        ],
        "duration": 1,
        "cooldown": "Short Rest",
        "cost": "6m movement speed"
      }
    ]
  },
  "spells": {
    "cantrips": [
      {
        "name": "Mage Hand",
        "damage": null,
        "description": "Create a spectral hand that can manipulate and interact with objects",
        "duration": 10,
        "range": 18,
        "cooldown": "Short Rest",
        "action": "Action",
        "concentration": false
      },
      {
        "name": "Poison Spray",
        "damage": "2-24 Poison",
        "description": "Project a puff of noxious gas",
        "duration": null,
        "range": 3,
        "cooldown": null,
        "action": "Action",
        "concentration": false
      },
      {
        "name": "True Strike",
        "damage": null,
        "description": "Gain Advantage on your next Attack Roll",
        "duration": 2,
        "range": 18,
        "cooldown": null,
        "action": "Action",
        "concentration": true
      },
      {
        "name": "Shocking Grasp",
        "damage": "2-16 Lightning",
        "description": "The target cannot use reactions. This spell has Advantage on creatures with metal armour",
        "duration": 1,
        "range": 0,
        "cooldown": null,
        "action": "Action",
        "concentration": false
      },
      {
        "name": "Friends",
        "damage": null,
        "description": "Gain Advantage on Charisma Checks against a non-hostile creature",
        "duration": 10,
        "range": 9,
        "cooldown": null,
        "action": "Action",
        "concentration": true
      }
    ],
    "metamagic": [
      {
        "name": "Twinned Spell",
        "effect": [
          "Spells that only target 1 creature can target an additional creature",
          "Costs 1 Sorcery point per spell slot level used. Cantrips also cost 1 Sorcery point"
        ]
      },
      {
        "name": "Distant Spell",
        "effect": [
          "Increase the range of spells by 50%. Melee spells get a range of 9m",
          "Costs 1 Sorcery point per spell"
        ]
      },
      {
        "name": "Extended Spell",
        "effect": [
          "Double the duration of conditions, summons, and surfaces caused by spells",
          "Costs 1 Sorcery point per spell"
        ]
      }
    ],
    "class actions": [
      {
        "name": "Create Sorcery Points",
        "effect": "Spend spell slots to gain Sorcery Points",
        "action": "Bonus Action"
      },
      {
        "name": "Create Spell Slot",
        "effect": "Spend Sorcery Points to unlock a spell slot",
        "action": "Bonus Action"
      }
    ],
    "level 1": [
      {
        "name": "Thunderwave",
        "damage": "2-16 Thunder",
        "description": [
          "Release a wave of thunderous force that pushes away all creatures and objects",
          "On Save: Targets still take half damage"
        ],
        "duration": null,
        "area": 5,
        "range": null,
        "cooldown": null,
        "save": "Constitution",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Magic Missile",
        "damage": "6-15 Force",
        "description": [
          "Shoot 3 magical darts, each dealing 2-5 Force damage",
          "They always hit their target"
        ],
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Ice Knife",
        "damage": [
          "1-10 Piercing",
          "2-12 Cold"
        ],
        "description": [
          "Throw a shard of ice that deals 1-10 Piercing damage. It explodes and deals 2-12 Cold damage to anyone nearby.",
          "It leaves an ice surface",
          "This spell can be cast while you are Silenced",
          "On Miss: The shard of ice still explodes"
        ],
        "duration": 2,
        "area": 2,
        "range": 18,
        "cooldown": null,
        "save": "Dexterity",
        "concentration": false,
        "action": "Action"
      }
    ],
    "level 2": [
      {
        "name": "Thunderwave",
        "damage": "3-24 Thunder",
        "description": [
          "Release a wave of thunderous force that pushes away all creatures and objects",
          "On Save: Targets still take half damage"
        ],
        "duration": null,
        "area": 5,
        "range": null,
        "cooldown": null,
        "save": "Constitution",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Magic Missile",
        "damage": "8-20 Force",
        "description": [
          "Shoot 4 magical darts, each dealing 2-5 Force damage",
          "They always hit their target"
        ],
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Ice Knife",
        "damage": [
          "1-10 Piercing",
          "3-18 Cold"
        ],
        "description": [
          "Throw a shard of ice that deals 1-10 Piercing damage. It explodes and deals 2-12 Cold damage to anyone nearby.",
          "It leaves an ice surface",
          "This spell can be cast while you are Silenced",
          "On Miss: The shard of ice still explodes"
        ],
        "duration": 2,
        "area": 2,
        "range": 1,
        "cooldown": null,
        "save": "Dexterity",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Cloud of Daggers",
        "damage": "4-16 Slashing",
        "description": "Conjure a cloud of spinning daggers that attack anyone inside",
        "duration": 10,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": true,
        "action": "Action"
      },
      {
        "name": "Scorching Ray",
        "damage": "6-36 Fire",
        "description": "Hurl 3 rays of fire. Each ray deals 2-12 Fire damage",
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": false,
        "action": "Action"
      }
    ],
    "level 3": [
      {
        "name": "Thunderwave",
        "damage": "4-32 Thunder",
        "description": [
          "Release a wave of thunderous force that pushes away all creatures and objects",
          "On Save: Targets still take half damage"
        ],
        "duration": null,
        "area": 5,
        "range": null,
        "cooldown": null,
        "save": "Constitution",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Magic Missile",
        "damage": "10-25 Force",
        "description": [
          "Shoot 5 magical darts, each dealing 2-5 Force damage",
          "They always hit their target"
        ],
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Ice Knife",
        "damage": [
          "1-10 Piercing",
          "4-24 Cold"
        ],
        "description": [
          "Throw a shard of ice that deals 1-10 Piercing damage. It explodes and deals 2-12 Cold damage to anyone nearby.",
          "It leaves an ice surface",
          "This spell can be cast while you are Silenced",
          "On Miss: The shard of ice still explodes"
        ],
        "duration": 2,
        "area": 2,
        "range": 18,
        "cooldown": null,
        "save": "Dexterity",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Cloud of Daggers",
        "damage": "6-24 Slashing",
        "description": "Conjure a cloud of spinning daggers that attack anyone inside",
        "duration": 10,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": true,
        "action": "Action"
      },
      {
        "name": "Scorching Ray",
        "damage": "8-48 Fire",
        "description": "Hurl 4 rays of fire. Each ray deals 2-12 Fire damage",
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Charisma",
        "concentration": false,
        "action": "Action"
      },
      {
        "name": "Fireball",
        "damage": "8-48 Fire",
        "description": [
          "Shoot a bright flame from your fingers that explodes upon contact, torching everything in the vicinity",
          "On Save: Targets still take half damage"
        ],
        "duration": null,
        "area": null,
        "range": 18,
        "cooldown": null,
        "save": "Dexterity",
        "concentration": false,
        "action": "Action"
      }
    ],
    "passive": [
      {
        "name": "Tides of Chaos",
        "effect": [
          "Activate to gain Advantage on your next Attack Roll, Ability Check, or Saving Throw",
          "Increased chance of Wild Magic surge afterwards"
        ]
      }
    ]
  }
}