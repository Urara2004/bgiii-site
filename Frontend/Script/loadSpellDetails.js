let uniqueIDSpell;
let htmlSpell;
let buttonsSpell = [];

function loadSpellDetails(character, list) {
    for (let x in list) {
        for (let y in list[x]){
            uniqueIDSpell = character.attributes.name + "-" + x + "-" + list[x][y].name;
            htmlSpell = "<details class='spell " + x + "'><summary>" + list[x][y].name + "</summary><div id='spellBox'>";
            printSpellDetails(list[x][y]);
            htmlSpell += `<br><button type='button' class='editSpell' id="` + uniqueIDSpell + `-edit">Edit spell</button> <button type='button' class='deleteSpell' id="` + uniqueIDSpell + `-delete">Delete spell</button>`;
            buttonsSpell.push(uniqueIDSpell + "-edit");
            buttonsSpell.push(uniqueIDSpell + "-delete");
            document.getElementById('dataSpell').innerHTML += htmlSpell + "</details>";
        }
    }
    for (x in character.gear) {
        if (character.gear[x] !== null && character.gear[x].spell !== undefined) {
            spell = character.gear[x].spell;
            uniqueIDSpell = character.attributes.name + "-gearSpell-" + spell.name;
            htmlSpell = "<details class='spell gearSpell'><summary>" + spell.name + "</summary><div id='spellBox'>";
            printSpellDetails(spell);
            htmlSpell += `<br><button type='button' class='editSpell' id="` + uniqueIDSpell + `-edit">Edit spell</button> <button type='button' class='deleteSpell' id="` + uniqueIDSpell + `-delete">Delete spell</button>`;
            buttonsSpell.push(uniqueIDSpell + "-edit");
            buttonsSpell.push(uniqueIDSpell + "-delete");
            document.getElementById('dataSpell').innerHTML += htmlSpell + "</details>";
        }
    }
}

function printSpellDetails(list) {
    htmlTableSpell = "<table>";
    for (let item in list) {
        if (list[item] != null) {
            htmlTableSpell += "<tr><td>" + item.charAt(0).toUpperCase() + item.slice(1) + "</td><td>";
            if (Array.isArray(list[item])) {
                for (var i = 0; i < list[item].length; i++) {
                    htmlTableSpell += list[item][i];
                    if (i != (list[item].length - 1)) {
                        htmlTableSpell += "<hr>";
                    }
                }
            } else {
                htmlTableSpell += list[item];
            }
            htmlTableSpell += "</td></tr>";
        }
    }
    htmlSpell += htmlTableSpell + "</table>";
}

function getButtonListSpell() {
    return buttonsSpell;
}

function createSpellButton(id) {
    const split = id.split("-");
    const type = split[split.length - 1];
    const button = document.getElementById(id);
    switch (type) {
        // case "edit" : {

        // }
        case "delete" : {
            button.addEventListener('dblclick', async () => {
                const query = split[0] + "-" + split[1] + "-" + split[2];
                const data = await deleteSpell(query);
                location.reload(true);
            })
        }
    }
}