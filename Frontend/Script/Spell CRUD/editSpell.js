let editSpell = {};
let characterPatchSpell;

function patchSpell(id) {
    window.location.href = "#boxEditSpell"
    characterPatchSpell = id;
    console.log(id);
}

async function submitPatchSpell() {
    const methods = {
        method: 'PATCH',
        body: JSON.stringify(newSpell),
        headers: {
            'Content-type': 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/update/" + characterPatchSpell.split("-")[0] + "/spells/" + characterPatchSpell.split("-")[1], methods);
    if (response.ok) {
        const data = await response.json();
        window.location.href = "#";
    } else {
        throw new Error("HTTP status " + response.status);
    }
}