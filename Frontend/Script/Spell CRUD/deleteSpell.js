async function deleteSpell(id) {
    const info = id.split("-");
    console.log(info)
    const methods = {
        method: 'DELETE'
    }
    const response = await fetch("http://localhost:5000/delete/" + info[0] + "/spells/" + info[1] + "/" + info[2], methods);
    if (response.ok) {
        const data = await response.json();
        return data
    } else {
        throw new Error("HTTP status " + response.status)
    }
}