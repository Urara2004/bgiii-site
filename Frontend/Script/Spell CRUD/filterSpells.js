filterSpells('all');

function getSpells() {
    return document.getElementsByClassName("spell");
}

function filterSpells(c) {
    var x, i;
    x = getSpells();
    if (c == "all") {
        c = "";
    }
    for (i = 0; i < x.length; i++) {
        removeSpell(x[i]);
        if (x[i].className.indexOf(c) > -1) {
            addSpell(x[i]);
        }
    }
}

function removeSpell(element) {
    element.className += " hidden";
}

function addSpell(element) {
    element.classList.remove("hidden");
}