let newSpell = {};
let characterPostSpell;

function createNewSpell(id) {
    window.location.href = "#boxCreateSpell";
    characterPostSpell = id;
}

async function submitNewSpell() {
    getAllSpellData();
    const methods = {
        method: 'POST',
        body: JSON.stringify(newSpell),
        headers: {
            'Content-type' : 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/add/" + characterPostSpell.split("-")[0] + "/spells/" + newSpell.type.charAt(0).toLowerCase() + newSpell.type.slice(1), methods)
    if (response.status == 201) {
        const data = await response.json();
        window.location.href = "#";
    } else {
        throw new Error("HTTP status " + response.status)
    }
}

function getAllSpellData() {
    let keyID;
    let formData = Array.from(document.querySelector('#newSpell').elements);
    for (let x in formData) {
        if (formData[x].value !== "" && formData[x].type !== "button" && formData[x].id !== "damageMinSpellCreate" && formData[x].id !== "damageMaxSpellCreate" && formData[x].id !== "damageTypeSpellCreate") {
            keyID = formData[x].id.replace("SpellCreate", "")
            newSpell[keyID] = formData[x].value;
        }
    }
    // if (formData[formData.indexOf("damageMinSkillCreate")].value !== "") {
    //     newSkill.damage = formData[formData.length - 5].value + "-" + formData[formData.length - 4].value + " " + formData[formData.length - 3].value;
    // }
}