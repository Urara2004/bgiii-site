let uniqueIDSkill;
let htmlSkill;
let buttonsSkill = [];

function loadSkillDetails(character, list) {
    for (let x in list) {
        for (let y in list[x]){
            uniqueIDSkill = character.attributes.name + "-" + x + "-" + list[x][y].name;
            htmlSkill = "<details class='skill " + x + "'><summary>" + list[x][y].name + "</summary><div id='skillBox'>";
            printSkillDetails(list[x][y]);
            htmlSkill += `<br><button type='button' class='editSkill' id="` + uniqueIDSkill + `-edit">Edit skill</button> <button type='button' class='deleteSkill' id="` + uniqueIDSkill + `-delete">Delete skill</button>`;
            buttonsSkill.push(uniqueIDSkill + "-edit");
            buttonsSkill.push(uniqueIDSkill + "-delete");
            document.getElementById('dataSkill').innerHTML += htmlSkill + "</details>"
        }
    }
    for (x in character.gear) {
        if (character.gear[x] !== null && character.gear[x].skill !== undefined) {
            skill = character.gear[x].skill;
            uniqueIDSkill = character.attributes.name + "-gearSkill-" + skill.name;
            htmlSkill = "<details class='skill gearSkill'><summary>" + skill.name + "</summary><div id='spellBox'>";
            printSkillDetails(skill)
            htmlSkill += `<br><button type='button' class='editSkill' id="` + uniqueIDSkill + `-edit">Edit skill</button> <button type='button' class='deleteSkill' id="` + uniqueIDSkill + `-delete">Delete skill</button>`;
            buttonsSkill.push(uniqueIDSkill + "-edit");
            buttonsSkill.push(uniqueIDSkill + "-delete");
            document.getElementById('dataSkill').innerHTML += htmlSkill + "</details>";
        }
    }
}

function printSkillDetails(list) {
    htmlTableSkill = "<table>";
    for (let item in list) {
        if (list[item] != null) {
            htmlTableSkill += "<tr><td>" + item.charAt(0).toUpperCase() + item.slice(1) + "</td><td>";
            if (Array.isArray(list[item])) {
                for (var i = 0; i < list[item].length; i++) {
                    htmlTableSkill += list[item][i];
                    if (i != (list[item].length - 1)) {
                        htmlTableSkill += "<hr>";
                    }
                }
            } else {
                htmlTableSkill += list[item];
            }
            htmlTableSkill += "</td></tr>";
        }
    }
    htmlSkill += htmlTableSkill + "</table>";
}

function getButtonListSkill() {
    return buttonsSkill;
}

function createSkillButton(id) {
    const split = id.split("-");
    const type = split[split.length - 1];
    const button = document.getElementById(id);
    switch (type) {
        // case "edit" : {

        // }
        case "delete" : {
            button.addEventListener('dblclick', async () => {
                const query = split[0] + "-" + split[1] + "-" + split[2];
                const data = await deleteSkill(query);
                location.reload(true);
            })
        }
    }
}