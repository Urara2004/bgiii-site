let uniqueIDGear;
let htmlGear;
let htmlKeyGear;
let htmlValueGear;
let buttonsGear = [];

function loadGearDetails(character, list) {
    for (let x in list) {
        uniqueIDGear = character.attributes.name + "-" + x + "-" + x.name;
        htmlGear = "<details id='gear' class='gear'><summary>" + x.charAt(0).toUpperCase() + x.slice(1) + "</summary><div id='gearBox'>";
        if (list[x] == null) {
            gearDoesNotExist(character, x);
            buttonsGear.push(uniqueIDGear + "-create");
        } else {
            gearDoesExist(character, x, list)
            htmlGear += `</div><br><button type='button' class='editGear' id="` + uniqueIDGear + `-edit">Edit gear</button> <button type='button' class='deleteGear' id="` + uniqueIDGear + `-delete">Delete gear</button>`;
            buttonsGear.push(uniqueIDGear + "-edit");
            buttonsGear.push(uniqueIDGear + "-delete");
        }
        document.getElementById('dataGear').innerHTML += htmlGear + "</details>";
    }
}

function getButtonListGear() {
    return buttonsGear;
}

function createGearButton(id) {
    const split = id.split("-");
    const type = split[split.length - 1];
    const button = document.getElementById(id);
    switch (type) {
        // case "create" : {
        //     button.addEventListener('dblclick', async () => {
        //     })
        // }
        case "edit" : {
            button.addEventListener('dblclick', () => {
                patchGear(split[0]);
            })
        }
        case "delete" : {
            button.addEventListener('dblclick', async () => {
                const query = split[0] + "-" + split[1] + "-" + split[2];
                const data = await deleteGear(query);
                location.reload(true);
            })
        }
    }
}

function gearDoesNotExist(character, gear) {
    uniqueIDGear = character.attributes.name + "-" + gear;
    htmlGear += "This part is unequipped" + "<button type='button' class='createNewGear' id='" + uniqueIDGear + `-create' ondblclick='createNewGear("` + uniqueIDGear + `")'>Add gear</button><br>`;
}

function gearDoesExist(character, gear, list) {
    uniqueIDGear = character.attributes.name + "-" + gear + "-" + list[gear].name;
    let ability = "";
    htmlTableGear = "<table>";
    for (let item in list[gear]) {
        if (item != 'skill' && item != 'spell') {
            htmlTableGear += "<tr><td>" + item.charAt(0).toUpperCase() + item.slice(1) + "</td><td>";
            if (Array.isArray(list[gear][item])) {
                for (var i = 0; i < list[gear][item].length; i++) {
                    htmlTableGear += list[gear][item][i];
                    if (i != (list[gear][item].length - 1)) {
                        htmlTableGear += "<hr>";
                    }
                }
            } else {
                htmlTableGear += list[gear][item];
            }
            htmlTableGear += "</td></tr>"
        } else {
            ability = item;
        }
    }
    htmlGear += htmlTableGear + "</table></div>";
    // htmlKeyGear = "<div id='gearKey'>";
    // let ability = "";
    // for (let key in list[gear]) {
    //     if (key != 'skill' && key != 'spell') {
    //         htmlKeyGear += key.charAt(0).toUpperCase() + key.slice(1) + "<br>";
    //     } else {
    //         ability = key;
    //     }
    // }
    // htmlGear += htmlKeyGear + "</div>";
    // htmlValueGear = "<div id='gearValue'>";
    // const gearSingle = list[gear];
    // for (let value in gearSingle) {
    //     if (gearSingle[value] instanceof Object && !Array.isArray(gearSingle[value])) {}
    //     else if (Array.isArray(gearSingle[value])) {
    //         for (var i = 0; i < gearSingle[value].length; i++) {
    //             htmlValueGear += gearSingle[value][i] + "<hr>";
    //         }
    //     } else if (!Array.isArray(gearSingle[value])) {
    //         htmlValueGear += gearSingle[value] + "<br>";
    //     }
    // }
    // htmlGear += htmlValueGear + "</div></div>";
    if (ability != "") {
        loadSpecific(ability, list[gear])
    }
}

function loadSpecific(key, item) {
    htmlGear += "<div id='gearAbility'>";
    if (key == 'skill') {
        htmlGear += "<details id='gearSkill' class='gear'><summary>" + key.charAt(0).toUpperCase() + key.slice(1) + "</summary><div id='gearBox'>";
        loadGearSkill(item.skill);
    } else if (key == 'spell') {
        htmlGear += "<details id='gearSpell' class='gear'><summary>" + key.charAt(0).toUpperCase() + key.slice(1) + "</summary><div id='gearBox'>";
        loadGearSpell(item.spell);
    }
}

function loadGearSkill(skill) {
    htmlGear += "<table class='gearAbility'>";
    for (let item in skill) {
        htmlGear += "<tr><td>" + item.charAt(0).toUpperCase() + item.slice(1) + "</td><td>" + skill[item] + "</td></tr>";
    }
    htmlGear += "</table></details>";
}

function loadGearSpell(spell) {
    htmlGear += "<table class='gearAbility'>";
    for (let item in spell) {
        htmlGear += "<tr><td>" + item.charAt(0).toUpperCase() + item.slice(1) + "</td><td>" + spell[item] + "</td></tr>";
    }
    htmlGear += "</table>";
}