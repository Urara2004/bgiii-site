let editGear = {};
let characterPatchGear;

function patchGear(id) {
    window.location.href = "#boxEditGear"
    characterPatchGear = id;
    console.log(id)
}

async function submitPatchGear() {
    const methods = {
        method: 'PATCH',
        body: JSON.stringify(newGear),
        headers: {
            'Content-type': 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/update/" + characterPatchGear.split("-")[0] + "/gear/" + characterPatchGear.split("-")[1], methods);
    if (response.ok) {
        const data = await response.json();
        window.location.href = "#";
    } else {
        throw new Error("HTTP status " + response.status)
    }
}