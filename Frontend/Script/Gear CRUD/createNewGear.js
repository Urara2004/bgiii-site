let newGear = {};
let characterPostGear;

function createNewGear(id) {
    window.location.href = "#boxCreateGear";
    characterPostGear = id;
}

function createNewGearSpell() {
    window.location.href = "#createGearSpell";
}

function createNewGearSkill() {
    window.location.href = "#createGearSkill";
}

async function submitNewGear() {
    getAllGearData()
    const methods = {
        method: 'POST',
        body: JSON.stringify(newGear),
        headers: {
            'Content-type' : 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/add/" + characterPostGear.split("-")[0] + "/gear/" + characterPostGear.split("-")[1], methods)
    if (response.status == 201) {
        const data = await response.json();
        window.location.href = "#";
        // return data;
    } else {
        throw new Error("HTTP status " + response.status)
    }
}

function submitNewGearSpecial(category) {
    let keyID;
    let formData = [];
    switch (category) {
        case "skill" : {
            formData = Array.from(document.querySelector("#newGearSkill").elements);
        }
        case "spell" : {
            formData = Array.from(document.querySelector("#newGearSpell").elements);
        }
        newGear[category] = {};
    }
    for (let x in formData) {
        console.log(formData[x]);
        if (formData[x].value !== "") {
            switch (category) {
                case "skill" : {
                    keyID = formData[x].id.replace("SkillGearCreate", "");
                }
                case "spell" : {
                    keyID = formData[x].id.replace("SpellGearCreate", "");
                }
                newGear[category][keyID] = formData[x].value
            }
        }
    }
    window.location.href = "#boxCreateGear";
}

function getAllGearData() {
    let keyID;
    let formData = Array.from(document.querySelector('#newGear').elements);
    for (let x in formData) {
        console.log(formData[x])
        if (formData[x].value !== "" && formData[x].type !== "button" && formData[x].id !== "damageMinGearCreate" && formData[x].id !== "damageMaxGearCreate" && formData[x].id !== "damageTypeGearCreate") {
            keyID = formData[x].id.replace("GearCreate", "")
            newGear[keyID] = formData[x].value;
        }
    }
    if (formData[formData.length - 3].value !== "") {
        newGear.damage = formData[formData.length - 5].value + "-" + formData[formData.length - 4].value + " " + formData[formData.length - 3].value;
    }
}