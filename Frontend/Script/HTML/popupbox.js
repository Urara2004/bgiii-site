document.write(
    `<div id="boxCreateGear" class="modal">` +
        `<div class="content">` +
            `<form id="newGear" class="box" action="">` +
                `<div class="key">` +
                    `<label for="nameGearCreate">Name: </label><br>` +
                    `<label for="armourClassGearCreate">Armour Class: </label><br>` +
                    `<label for="effectGearCreate">Effect: </label><br>` +
                    `<label for="damageMinGearCreate">Damage minimum: </label><br>` +
                    `<label for="damageMaxGearCreate">Damage maximum: </label><br>` +
                    `<label for="damageTypeGearCreate">Damage type: </label><br>` +
                    `<label for="spellCreateGear">Spell</label><br>` +
                    `<label for="skillCreateGear">Skill</label>` +
                `</div>` +
                `<div class="value">` +
                    `<input type="text" id="nameGearCreate" required><br>` +
                    `<input type="number" id="armourClassGearCreate"><br>` +
                    `<input type="text" id="effectGearCreate"><br>` +
                    `<input type="number" id="damageMinGearCreate"><br>` +
                    `<input type="number" id="damageMaxGearCreate"><br>` +
                    `<input type="text" id="damageTypeGearCreate"><br>` +
                    `<input type="button" id="spellCreateGear" value="Create Spell" ondblclick="createNewGearSpell()"><br>` +
                    `<input type="button" id="skillCreateGear" value="Create Skill" ondblclick="createNewGearSkill()">` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitNewGearButton">Submit</button>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="createGearSpell" class="modal">` +
        `<div class="content">` +
            `<form id="newGearSpell" class="box" action="">` +
                `<div class="key">` +
                    `<label for="nameSpellGearCreate">Name: </label><br>` +
                    `<label for="descriptionSpellGearCreate">Description: </label><br>` +
                    `<label for="damageMinSpellGearCreate">Damage minimum: </label><br>` +
                    `<label for="damageMaxSpellGearCreate">Damage maximum: </label><br>` +
                    `<label for="damageTypeSpellGearCreate">Damage type: </label><br>` +
                    `<label for="cooldownSpellGearCreate">Cooldown: </label><br>` +
                    `<label for="durationSpellGearCreate">Duration: </label><br>` +
                    `<label for="concentrationSpellGearCreate">Concentration: </label><br>` +
                    `<label for="rangeSpellGearCreate">Range: </label><br>` +
                    `<label for="actionSpellGearCreate">Action: </label>` +
                `</div>` +
                `<div class="value">` +
                    `<input type="text" id="nameSpellGearCreate" required><br>` +
                    `<input type="text" id="descriptionSpellGearCreate"><br>` +
                    `<input type="number" id="damageMinSpellGearCreate"><br>` +
                    `<input type="number" id="damageMaxSpellGearCreate"><br>` +
                    `<input type="text" id="damageTypeSpellGearCreate"><br>` +
                    `<select id="cooldownSpellGearCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Per Turn">Per Turn</option>` +
                        `<option value="Per Battle">Per Battle</option>` +
                        `<option value="Short Rest">Short Rest</option>` +
                        `<option value="Long Rest">Long Rest</option>` +
                    `</select><br>` +
                    `<input type="number" id="durationSpellGearCreate"><br>` +
                    `<select id="concentrationSpellGearCreate">` +
                        `<option value="False">False</option>` +
                        `<option value="True">True</option>` +
                    `</select><br>` +
                    `<input type="number" id="rangeSpellGearCreate"><br>` +
                    `<select id="actionSpellGearCreate">` +
                        `<option value="Action">Action</option>` +
                        `<option value="Bonus Action">Bonus Action</option>` +
                    `</select>` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitNewGearSpellButton" ondblclick="submitNewGearSpecial('spell')">Submit</button>` +
            `<a href="#boxCreateGear"` +
                `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="createGearSkill" class="modal">` +
        `<div class="content">` +
            `<form id="newGearSkill" class="box" action="">` +
                `<div class="key">` +
                    `<label for="nameSkillGearCreate">Name: </label><br>` +
                    `<label for="descriptionSkillGearCreate">Description: </label><br>` +
                    `<label for="damageMinSkillGearCreate">Damage minimum: </label><br>` +
                    `<label for="damageMaxSkillGearCreate">Damage maximum: </label><br>` +
                    `<label for="damageTypeSkillGearCreate">Damage type: </label><br>` +
                    `<label for="cooldownSkillGearCreate">Cooldown: </label><br>` +
                    `<label for="durationSkillGearCreate">Duration: </label><br>` +
                    `<label for="concentrationSkillGearCreate">Concentration: </label><br>` +
                    `<label for="rangeSkillGearCreate">Range: </label><br>` +
                    `<label for="actionSkillGearCreate">Action: </label>` +
                `</div>` +
                `<div class="value">` +
                    `<input type="text" id="nameSkillGearCreate" required><br>` +
                    `<input type="text" id="descriptionSkillGearCreate"><br>` +
                    `<input type="number" id="damageMinSkillGearCreate"><br>` +
                    `<input type="number" id="damageMaxSkillGearCreate"><br>` +
                    `<input type="text" id="damageTypeSkillGearCreate"><br>` +
                    `<select id="cooldownSkillGearCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Per Turn">Per Turn</option>` +
                        `<option value="Per Battle">Per Battle</option>` +
                        `<option value="Short Rest">Short Rest</option>` +
                        `<option value="Long Rest">Long Rest</option>` +
                    `</select><br>` +
                    `<input type="number" id="durationSkillGearCreate"><br>` +
                    `<select id="concentrationSkillGearCreate">` +
                        `<option value="False">False</option>` +
                        `<option value="True">True</option>` +
                    `</select><br>` +
                    `<input type="number" id="rangeSkillGearCreate"><br>` +
                    `<select id="actionSkillGearCreate">` +
                        `<option value="Action">Action</option>` +
                        `<option value="Bonus Action">Bonus Action</option>` +
                    `</select>` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitNewGearSkillButton" ondblclick="submitNewGearSpecial('skill')">Submit</button>` +
            `<a href="#boxCreateGear"` +
                `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="boxEditGear" class="modal">` +
        `<div class="content">` +
            `<form id="editGear" class="box" action="">` +
                `<div class="key">` +
                    `<label for="nameGearEdit">Name: </label><br>` +
                    `<label for="armourClassGearEdit">Armour Class: </label><br>` +
                    `<label for="effectGearEdit">Effect: </label><br>` +
                    `<label for="damageMinGearEdit">Damage minimum: </label><br>` +
                    `<label for="damageMaxGearEdit">Damage maximum: </label><br>` +
                    `<label for="damageTypeGearEdit">Damage type: </label><br>` +
                    `<label for="spellEditGear">Spell</label><br>` +
                    `<label for="skillEditGear">Skill</label>` +
                `</div>` +
                `<div class="value">` +
                    `<input type="text" id="nameGearEdit"><br>` +
                    `<input type="number" id="armourClassGearEdit"><br>` +
                    `<input type="text" id="effectGearEdit"><br>` +
                    `<input type="text" id="damageMinGearEdit"><br>` +
                    `<input type="text" id="damageMaxGearEdit"><br>` +
                    `<input type="text" id="damageTypeGearEdit"><br>` +
                    `<input type="button" id="spellEditGear" value="Edit Spell" ondblclick="editGearSpell()"><br>` +
                    `<input type="button" id="skillEditGear" value="Edit Skill" ondblclick="editGearSkill()">` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitEditGearButton">Submit</button>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="boxCreateSkill" class="modal">` +
        `<div class="content">` +
            `<form id="newSkill" class="box" action="">` +
                `<div class="key">` +
                    `<label for="typeSkillCreate">Type: </label><br>` +
                    `<label for="nameSkillCreate">Name: </label><br>` +
                    `<label for="damageMinSkillCreate">Damage minimum (` + /\n/ + `): </label><br>` +
                    `<label for="damageMaxSkillCreate">Damage maximum (` + /\n/ + `): </label><br>` +
                    `<label for="damageTypeSkillCreate">Damage type (` + /\n/ + `): </label><br>` +
                    `<label for="descriptionSkillCreate">Description (` + /\n/ + `): </label><br>` +
                    `<label for="durationSkillCreate">Duration: </label><br>` +
                    `<label for="areaSkillCreate">Area: </label><br>` +
                    `<label for="rangeSkillCreate">Range: </label><br>` +
                    `<label for="cooldownSkillCreate">Cooldown: </label><br>` +
                    `<label for="saveSkillCreate">Save: </label><br>` +
                    `<label for="concentrationSkillCreate">Concentration: </label><br>` +
                    `<label for="actionSkillCreate">Action: </label><br>` +
                    `<label for="costSkillCreate">Cost: </label><br>` +
                `</div>` +
                `<div class="value">` +
                    `<select id="typeSkillCreate">` +
                        `<option value="Melee">Melee</option>` +
                        `<option value="Ranged">Ranged</option>` +
                        `<option value="Passive">Passive</option>` +
                    `</select><br>` +
                    `<input type="text" id="nameSkillCreate" required><br>` +
                    `<input type="text" id="damageMinSkillCreate"><br>` +
                    `<input type="text" id="damageMaxSkillCreate"><br>` +
                    `<input type="text" id="damageTypeSkillCreate"><br>` +
                    `<input type="text" id="descriptionSkillCreate"><br>` +
                    `<input type="number" id="durationSkillCreate"><br>` +
                    `<input type="number" id="areaSkillCreate"><br>` +
                    `<input type="number" id="rangeSkillCreate"><br>` +
                    `<select id="cooldownSkillCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Per Turn">Per Turn</option>` +
                        `<option value="Per Fight">Per Fight</option>` +
                        `<option value="Short Rest">Short Rest</option>` +
                        `<option value="Long Rest">Long Rest</option>` +
                    `</select><br>` +
                    `<select id="saveSkillCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Strength">Strength</option>` +
                        `<option value="Dexterity">Dexterity</option>` +
                        `<option value="Constitution">Constitution</option>` +
                        `<option value="Intelligence">Intelligence</option>` +
                        `<option value="Wisdom">Wisdom</option>` +
                        `<option value="Charisma">Charisma</option>` +
                    `</select><br>` +
                    `<input type="checkbox" id="concentrationSkillCreate" can-value="Concentration" can-true-value="True" can-false-value="False"><br>` +
                    `<select id="actionSkillCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Action">Action</option>` +
                        `<option value="Bonus Action">Bonus Action</option>` +
                    `</select><br>` +
                    `<input type="text" id="costSkillCreate"><br>` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitNewSkillButton">Submit</button>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="boxEditSkill" class="modal">` +
        `<div class="content">` +
            `<h1>` +
                `Gear` +
            `</h1>` +
            `<b>` +
                `<p>Never Give Up !</p>` +
            `</b>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="boxCreateSpell" class="modal">` +
        `<div class="content">` +
            `<form id="newSpell" class="box" action="">` +
                `<div class="key">` +
                    `<label for="typeSpellCreate">Type: </label><br>` +
                    `<label for="nameSpellCreate">Name: </label><br>` +
                    `<label for="damageMinSpellCreate">Damage minimum (` + /\n/ + `): </label><br>` +
                    `<label for="damageMaxSpellCreate">Damage maximum (` + /\n/ + `): </label><br>` +
                    `<label for="damageTypeSpellCreate">Damage type (` + /\n/ + `): </label><br>` +
                    `<label for="descriptionSpellCreate">Description (` + /\n/ + `): </label><br>` +
                    `<label for="durationSpellCreate">Duration: </label><br>` +
                    `<label for="areaSpellCreate">Area: </label><br>` +
                    `<label for="rangeSpellCreate">Range: </label><br>` +
                    `<label for="cooldownSpellCreate">Cooldown: </label><br>` +
                    `<label for="saveSpellCreate">Save: </label><br>` +
                    `<label for="concentrationSpellCreate">Concentration: </label><br>` +
                    `<label for="actionSpellCreate">Action: </label><br>` +
                `</div>` +
                `<div class="value">` +
                    `<select id="typeSpellCreate">` +
                        `<option value="Cantrips">Cantrip</option>` +
                        `<option value="Passive">Passive</option>` +
                        `<option value="Level 1">Level 1</option>` +
                        `<option value="Level 2">Level 2</option>` +
                        `<option value="Level 3">Level 3</option>` +
                        `<option value="Class Actions">Class Actions</option>` +
                        `<option value="Metamagic">Metamagic</option>` +
                    `</select><br>` +
                    `<input type="text" id="nameSpellCreate" required><br>` +
                    `<input type="text" id="damageMinSpellCreate"><br>` +
                    `<input type="text" id="damageMaxSpellCreate"><br>` +
                    `<input type="text" id="damageTypeSpellCreate"><br>` +
                    `<input type="text" id="descriptionSpellCreate"><br>` +
                    `<input type="number" id="durationSpellCreate"><br>` +
                    `<input type="number" id="areaSpellCreate"><br>` +
                    `<input type="number" id="rangeSpellCreate"><br>` +
                    `<select id="cooldownSpellCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Per Turn">Per Turn</option>` +
                        `<option value="Per Fight">Per Fight</option>` +
                        `<option value="Short Rest">Short Rest</option>` +
                        `<option value="Long Rest">Long Rest</option>` +
                    `</select><br>` +
                    `<select id="saveSpellCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Strength">Strength</option>` +
                        `<option value="Dexterity">Dexterity</option>` +
                        `<option value="Constitution">Constitution</option>` +
                        `<option value="Intelligence">Intelligence</option>` +
                        `<option value="Wisdom">Wisdom</option>` +
                        `<option value="Charisma">Charisma</option>` +
                    `</select><br>` +
                    `<input type="checkbox" id="concentrationSpellCreate" can-value="Concentration" can-true-value="True" can-false-value="False"><br>` +
                    `<select id="actionSpellCreate">` +
                        `<option value="None">None</option>` +
                        `<option value="Action">Action</option>` +
                        `<option value="Bonus Action">Bonus Action</option>` +
                    `</select><br>` +
                `</div>` +
            `</form>` +
            `<br><br><button type="button" id="submitNewSpellButton">Submit</button>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)

document.write(
    `<div id="boxEditSpell" class="modal">` +
        `<div class="content">` +
            `<h1>` +
                `Gear` +
            `</h1>` +
            `<b>` +
                `<p>Never Give Up !</p>` +
            `</b>` +
            `<a href="#"` +
               `class="box-close">` +
                `x` +
            `</a>` +
        `</div>` +
    `</div>`
)