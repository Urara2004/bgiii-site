async function getCharacter(character) {
	const response = await fetch("http://localhost:5000/json/" + character + ".json");
	const data = await response.json();
	return data;
}

async function loadingCharacter() {
	let character;
	try {
		let url = location.href.substring(location.href.indexOf("?")+1)
		if (url.slice(-1) == "#") {
			url = url.slice(0, -1);
		}
		character = await getCharacter(url);
	} catch (error) {
		console.log(error);
	}
	console.log(character);
	loadAttributeCard(character);
	loadGearDetails(character, character.gear);
	loadSkillDetails(character, character.skills);
	loadSpellDetails(character, character.spells);
	loadAllButtons();
}

function loadAttributeCard(character) {
	document.getElementById('name').innerHTML = character.attributes.name;
	document.getElementById('level').innerHTML = character.attributes.level;
	document.getElementById('subrace').innerHTML = character.attributes.subrace;
	document.getElementById('class').innerHTML = character.attributes.class;
	document.getElementById('subclass').innerHTML = character.attributes.subclass;
}

function loadAllButtons() {
	let buttonsGear = getButtonListGear();
	let buttonsSkill = getButtonListSkill();
	let buttonsSpell = getButtonListSpell();
	for (let button in buttonsGear) {
		createGearButton(buttonsGear[button]);
	}
	for (let button in buttonsSkill) {
		createSkillButton(buttonsSkill[button]);
	}
	for (let button in buttonsSpell) {
		createSpellButton(buttonsSpell[button]);
	}
	document.getElementById("submitNewSkillButton").addEventListener('dblclick', async () => {
		const data = await submitNewSkill();
		location.reload(true);
	})
	document.getElementById("submitNewSpellButton").addEventListener('dblclick', async () => {
		const data = await submitNewSpell();
		location.reload(true);
	})
	document.getElementById("submitNewGearButton").addEventListener('dblclick', async () => {
        const data = await submitNewGear();
		location.reload(true);
    })
}