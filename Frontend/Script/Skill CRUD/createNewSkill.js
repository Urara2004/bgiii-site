let newSkill = {};
let characterPostSkill;

function createNewSkill(id) {
    window.location.href = "#boxCreateSkill";
    characterPostSkill = id;
    console.log(characterPostSkill);
}

async function submitNewSkill() {
    getAllSkillData();
    const methods = {
        method: 'POST',
        body: JSON.stringify(newSkill),
        headers: {
            'Content-type' : 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/add/" + characterPostSkill.split("-")[0] + "/skills/" + newSkill.type.charAt(0).toLowerCase() + newSkill.type.slice(1), methods)
    if (response.status == 201) {
        const data = await response.json();
        window.location.href = "#";
    } else {
        throw new Error("HTTP status " + response.status)
    }
}

function getAllSkillData() {
    let keyID;
    let formData = Array.from(document.querySelector('#newSkill').elements);
    for (let x in formData) {
        if (formData[x].value !== "" && formData[x].type !== "button" && formData[x].id !== "damageMinSkillCreate" && formData[x].id !== "damageMaxSkillCreate" && formData[x].id !== "damageTypeSkillCreate") {
            keyID = formData[x].id.replace("SkillCreate", "")
            newSkill[keyID] = formData[x].value;
        }
    }
    // if (formData[formData.indexOf("damageMinSkillCreate")].value !== "") {
    //     newSkill.damage = formData[formData.length - 5].value + "-" + formData[formData.length - 4].value + " " + formData[formData.length - 3].value;
    // }
}