filterSkills('all');

function getSkills() {
    return document.getElementsByClassName("skill");
}

function filterSkills(c) {
    var x, i;
    x = getSkills();
    if (c == "all") {
        c = "";
    }
    for (i = 0; i < x.length; i++) {
        removeSkill(x[i]);
        if (x[i].className.indexOf(c) > -1) {
            addSkill(x[i]);
        }
    }
}

function removeSkill(element) {
    element.className += " hidden";
}

function addSkill(element) {
    element.classList.remove("hidden");
}