let editSkill = {};
let characterPatchSkill;

function patchSkill(id) {
    window.location.href = "#boxEditSkill"
    characterPatchSkill = id;
    console.log(id);
}

async function submitPatchSkill() {
    const methods = {
        method: 'PATCH',
        body: JSON.stringify(newSkill),
        headers: {
            'Content-type': 'application/json'
        }
    }
    const response = await fetch("http://localhost:5000/update/" + characterPatchSkill.split("-")[0] + "/skills/" + characterPatchSkill.split("-")[1]);
    if (response.ok) {
        const data = await response.json();
        window.location.href = "#";
    } else {
        throw new Error("HTTP status " + response.status);
    }
}